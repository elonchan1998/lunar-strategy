# %%
import pandas as pd
import os
import statsmodels.api as sm
import pylunar

# %%
filenames = [file for file in os.listdir('.') if file.endswith('csv')]

# %%

report_stat = []

for i, filename in enumerate(filenames):
    country_name = filename.replace('.csv', '')

    data = pd.read_csv(filename, parse_dates=['Date'])
    data = data.iloc[::-1]
    data.set_index('Date', inplace=True)
    data['Price'] = data['Price'].astype(str).str.replace(',', '').astype(float)
    data['Return'] = data['Price']/data['Price'].shift(1) - 1

    data.dropna(inplace=True)
    starting_date = data.index[0]
    ending_date = data.index[-1]
    num_obs = len(data)
    mean_return = data['Return'].mean()
    std_return = data['Return'].std()

    report_stat.append([country_name, starting_date, ending_date, num_obs, mean_return, std_return])

report_stat = pd.DataFrame(report_stat)
report_stat.columns = ['Country', 'Starting Date', 'Ending Date', 'Number of Observation', 'Mean of Daily Return', 'Std of Daily Return']

# %%
report_stat.sort_values(['Country'], inplace=True)
report_stat.set_index(['Country'], inplace=True)


# %%

f = open('summary_stat.tex', 'w')
f.write(report_stat.to_latex())
f.close()

# %%
report_beta = []

def get_phase(x):
    mi.update(x)
    return mi.fractional_phase()

mi = pylunar.MoonInfo((22, 28, 55), (114, 15, 77))  # Hong Kong

df_date = pd.DataFrame()
df_date['Date'] = pd.date_range('2000-01-01', '2020-03-29')
df_date['Lunar'] = df_date['Date'].apply(get_phase)
df_date.set_index('Date', inplace=True)

for i, filename in enumerate(filenames):
    country_name = filename.replace('.csv', '')

    data = pd.read_csv(filename, parse_dates=['Date'])
    data = data.iloc[::-1]
    data.set_index('Date', inplace=True)
    data['Price'] = data['Price'].astype(str).str.replace(',', '').astype(float)
    data['Return'] = data['Price']/data['Price'].shift(1) - 1

    data.dropna(inplace=True)

    df = pd.DataFrame(columns=['Lunar'])
    df['Lunar'] = df_date['Lunar']
    df = df.join(data, how='left')

    df.dropna(inplace=True)

    X = df['Lunar']
    X = sm.add_constant(X)

    res = sm.OLS(df['Return'], X).fit()
    const_beta = res.params['const']
    var_beta = res.params['Lunar']
    const_p = res.pvalues['const']
    var_p = res.pvalues['Lunar']

    report_beta.append([country_name, const_beta, const_p, var_beta, var_p])

report_beta = pd.DataFrame(report_beta)
report_beta.columns = ['Country', 'Coefficient', 'p-value', 'Coefficient', 'p-value']

# %%

report_beta.sort_values(['Country'], inplace=True)
report_beta.set_index(['Country'], inplace=True)

f = open('lunar_beta.tex', 'w')
f.write(report_beta.to_latex())
f.close()

# %%
# Volume
report_stat = []

for i, filename in enumerate(filenames):
    country_name = filename.replace('.csv', '')

    data = pd.read_csv(filename, parse_dates=['Date'])
    data = data.iloc[::-1]
    data.set_index('Date', inplace=True)
    data = data[data['Vol.'] != '-']
    data['Vol.'] = data['Vol.'].str.replace('.', '')
    data['Vol.'] = data['Vol.'].str.replace(',', '')
    data['Vol.'] = data['Vol.'].str.replace('K', '0')
    data['Vol.'] = data['Vol.'].str.replace('M', '0000')
    data['Vol.'] = data['Vol.'].str.replace('B', '0000000')
    data['Vol.'] = data['Vol.'].astype(int)
    data['Change_Vol'] = data['Vol.']/data['Vol.'].shift(1) - 1

    data.dropna(inplace=True)
    if data.empty:
        continue
    starting_date = data.index[0]
    ending_date = data.index[-1]
    num_obs = len(data)
    mean_change_vol = data['Change_Vol'].mean()
    std_change_vol = data['Change_Vol'].std()

    report_stat.append([country_name, starting_date, ending_date, num_obs, mean_change_vol, std_change_vol])

report_stat = pd.DataFrame(report_stat)
report_stat.columns = ['Country', 'Starting Date', 'Ending Date', 'Number of Observation', 'Mean of Daily \% Change in Volume', 'Std of Daily % Change in Volume']

# %%
report_stat.sort_values(['Country'], inplace=True)
report_stat.set_index(['Country'], inplace=True)
f = open('summary_stat_vol.tex', 'w')
f.write(report_stat.to_latex())
f.close()

# %%

report_beta = []

def get_phase(x):
    mi.update(x)
    return mi.fractional_phase()

mi = pylunar.MoonInfo((22, 28, 55), (114, 15, 77))  # Hong Kong

df_date = pd.DataFrame()
df_date['Date'] = pd.date_range('2000-01-01', '2020-03-29')
df_date['Lunar'] = df_date['Date'].apply(get_phase)
df_date.set_index('Date', inplace=True)

for i, filename in enumerate(filenames):
    country_name = filename.replace('.csv', '')

    data = pd.read_csv(filename, parse_dates=['Date'])
    data = data.iloc[::-1]
    data.set_index('Date', inplace=True)
    data = data[data['Vol.'] != '-']
    data['Vol.'] = data['Vol.'].str.replace('.', '')
    data['Vol.'] = data['Vol.'].str.replace(',', '')
    data['Vol.'] = data['Vol.'].str.replace('K', '0')
    data['Vol.'] = data['Vol.'].str.replace('M', '0000')
    data['Vol.'] = data['Vol.'].str.replace('B', '0000000')
    data['Vol.'] = data['Vol.'].astype(int)
    data['Change_Vol'] = data['Vol.']/data['Vol.'].shift(1) - 1

    data.dropna(inplace=True)

    if data.empty:
        continue

    df = pd.DataFrame(columns=['Lunar'])
    df['Lunar'] = df_date['Lunar']
    df = df.join(data, how='left')

    df.dropna(inplace=True)

    X = df['Lunar']
    X = sm.add_constant(X)

    res = sm.OLS(df['Change_Vol'], X).fit()
    const_beta = res.params['const']
    var_beta = res.params['Lunar']
    const_p = res.pvalues['const']
    var_p = res.pvalues['Lunar']

    report_beta.append([country_name, const_beta, const_p, var_beta, var_p])

report_beta = pd.DataFrame(report_beta)
report_beta.columns = ['Country', 'Coefficient', 'p-value', 'Coefficient', 'p-value']

# %%

report_beta.sort_values(['Country'], inplace=True)
report_beta.set_index(['Country'], inplace=True)

f = open('lunar_beta_vol.tex', 'w')
f.write(report_beta.to_latex())
f.close()

# %%
with open('Japan.csv', 'r') as f:
    data = pd.read_csv(f, parse_dates=['Date'])

    data = data.iloc[::-1]
    data.set_index('Date', inplace=True)
    data = data[data['Vol.'] != '-']
    print(data.loc['2015-09-10'])
    print(data.loc['2015-09-11'])
    data['Vol.'] = data['Vol.'].str.replace('.', '')
    data['Vol.'] = data['Vol.'].str.replace(',', '')
    data['Vol.'] = data['Vol.'].str.replace('K', '0')
    data['Vol.'] = data['Vol.'].str.replace('M', '0000')
    data['Vol.'] = data['Vol.'].str.replace('B', '0000000')
    data['Vol.'] = data['Vol.'].astype(int)
    data['Change_Vol'] = data['Vol.']/data['Vol.'].shift(1) - 1
    print(data.loc['2015-09-10'])
    print(data.loc['2015-09-11'])
    print(data.loc['2015-09-14'])
# %%
