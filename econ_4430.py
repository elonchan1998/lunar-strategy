# %%
import numpy as np
import statsmodels.api as sm
import linearmodels as lm
import pandas as pd
import pylunar
import math
import matplotlib.pyplot as plt
import seaborn as sns

# %%
df = pd.read_csv("Australia.csv", header=0, parse_dates=True)

# Make sure the Date column is in right format
df['Date'] = pd.to_datetime(df['Date'])

# check if need to reverse the df
if df['Date'][0] > df['Date'][1]:
    df = df[::-1]
    df.reset_index(drop=True, inplace=True)

# %%
df_date = pd.DataFrame()
df_date['Date'] = pd.date_range(start='2000-1-1', end='2020-3-6', freq='D')

# %%


def get_phase(x):
    mi.update(x)
    return mi.fractional_phase()


mi = pylunar.MoonInfo((22, 28, 55), (114, 15, 77))  # Hong Kong
mi = pylunar.MoonInfo((40, 73, 6), (-73, 93, 52))  # Boston
mi = pylunar.MoonInfo((-18, 1, 27), (31, 7, 55))  # INDZI
mi = pylunar.MoonInfo((-33, 87, 4), (151, 20, 87)) # Sydney

df_date['Phase'] = df_date['Date'].apply(get_phase)
df_date["Phase_name"] = 'Other'
df_date.loc[(df_date['Phase'] > df_date['Phase'].shift(1)) & (df_date['Phase'] > df_date['Phase'].shift(-1)), 'Phase_name'] = 'FULL_MOON'
df_date.loc[(df_date['Phase'] < df_date['Phase'].shift(1)) & (df_date['Phase'] < df_date['Phase'].shift(-1)), 'Phase_name'] = 'NEW_MOON'

df_date.loc[df_date['Phase_name'] == 'FULL_MOON', 'd'] = np.NaN
j = 1
for i in range(0, len(df_date)):
    if df_date['Phase_name'][i] == 'FULL_MOON':
        df_date['d'][i] = 0
        j = 1
    else:
        df_date['d'][i] = j
        j += 1

df = pd.merge(df, df_date, how='left')
# %%

try:
    df['Ln_return'] = (df['Close']/df['Close'].shift(1)).apply(math.log)  # log return natural log
except: # handle data from investing.com
    try:
        df['Price'] = df['Price'].str.replace(',', '')
    except:
        pass
    df['Price'] = df['Price'].astype(float)
    df['Ln_return'] = (df['Price']/df['Price'].shift(1)).apply(math.log)  # log return natural log

df = df[(df.d == 0).idxmax():] # remove all before first full moon
df.dropna(inplace=True)
df.reset_index(drop=True, inplace=True)

# %%
# Try to replicate paper's first regression
X = np.cos((2*np.pi)*df['d']/29.53)
X = sm.add_constant(X)
model = sm.OLS(df['Ln_return'], X)
res = model.fit()
print(res.summary())
# %%

# Second regression: Natural log return on moon phase
X = df['Phase']
X = sm.add_constant(X)
model = sm.OLS(df['Ln_return'], X)
res = model.fit()
print(res.summary())

# %%

# Creating full moon dummy, 1 if full moon, 0 if new moon, drop otherwise

conditions = [(df['Phase'] >= 0.8), (df['Phase'] <= 0.2)]
choices = [1, 0]

df['Full_Moon_Period'] = np.select(conditions, choices, default=np.NaN)

df_pool = df.dropna()
df_pool.reset_index(inplace=True, drop=True)

# %%

# Third regression: Natural log return on FULL_Moon Dummy

X = df_pool['Full_Moon_Period'].to_numpy()
X = sm.add_constant(X)
model = sm.OLS(df_pool['Ln_return'], X)
res = model.fit()
print(res.summary())

# %%

# Panel data. average log return on full moon or new moon
moon_avg_return = []
moon_return = 0
count = 0.0
full_moon = 1 if df['Full_Moon_Period'][0] == 1.0 else 0.0
for i in range(1, len(df_pool)):
    if df_pool['Full_Moon_Period'][i] != df_pool['Full_Moon_Period'][i-1]:
        moon_avg_return.append([moon_return/count, full_moon])
        count = 0
        moon_return = 0
        full_moon = 1 if full_moon == 0 else 0
    moon_return += df['Ln_return'][i]
    count += 1.0

moon_avg_return = np.asarray(moon_avg_return)

# Forth regression: regression 2 on the paper

X = moon_avg_return[:, 1]
X = sm.add_constant(X)
model = sm.OLS(moon_avg_return[:, 0], X)
res = model.fit()
print(res.summary())

# %%
# Panel regression



# %%

# Changing the dependent variable to Volume
try:
    df = df[df['Volume'] != 0]  # some early entry didn't record volume
except:
    df['Volume'] = df['Vol.']
    df['Volume'] = df['Volume'].str.replace('-', '0')
    df['Volume'] = df['Volume'].str.replace('.', '')
    df['Volume'] = df['Volume'].str.replace('M', '0000000')
    df['Volume'] = df['Volume'].str.replace('B', '0000000000')
    df['Volume'] = df['Volume'].astype(float)
    df = df[df['Volume'] != 0]

df['Ln_volume'] = (df['Volume']/df['Volume'].shift(1)).apply(math.log)  # log return natural log
df['Phase'] = df['Date'].apply(get_phase)

df["Phase_name"] = 'Other'
df.loc[(df['Phase'] > df['Phase'].shift(1)) & (df['Phase'] > df['Phase'].shift(-1)), 'Phase_name'] = 'FULL_MOON'
df.loc[(df['Phase'] < df['Phase'].shift(1)) & (df['Phase'] < df['Phase'].shift(-1)), 'Phase_name'] = 'NEW_MOON'

df.dropna(inplace=True)
df.reset_index(inplace=True)


# %%
X = df['Phase']
X = sm.add_constant(X)
model = sm.OLS(df['Ln_volume'], X)
res = model.fit()
print(res.summary())

# %%
X = df_pool['Full_Moon_Period'].to_numpy()
X = sm.add_constant(X)
model = sm.OLS(df_pool['Ln_volume'], X)
res = model.fit()
print(res.summary())

# %%

moon_avg_vol = []
moon_vol = 0
count = 0.0
full_moon = 1
for i in range(1, len(df_pool)):
    if df_pool['Full_Moon_Period'][i] != df_pool['Full_Moon_Period'][i-1]:
        moon_avg_vol.append([moon_vol/count, full_moon])
        count = 0
        moon_vol = 0
        full_moon = 1 if full_moon == 0 else 0
    moon_vol += df['Ln_volume'][i]
    count += 1.0

moon_avg_vol = np.asarray(moon_avg_vol)

X = moon_avg_vol[:, 1]
X = sm.add_constant(X)
model = sm.OLS(moon_avg_vol[:, 0], X)
res = model.fit()
print(res.summary())




# %%
# pool regression

import glob

path = "./"
all_files = glob.glob(os.path.join(path, "*.csv"))

li = []

for filename in all_files:
    df = pd.read_csv(filename, index_col=None, header=0)
    li.append(df)

frame = pd.concat(li, axis=0, ignore_index=True)

# %%
