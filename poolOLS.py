# %%
from linearmodels import PanelOLS
import pandas as pd
import os
from linearmodels import RandomEffects
import pylunar

# %%
filenames = [file for file in os.listdir('.') if file.endswith('csv')]

def get_phase(x):
    mi.update(x)
    return mi.fractional_phase()

mi = pylunar.MoonInfo((22, 28, 55), (114, 15, 77))  # Hong Kong

df_date = pd.DataFrame()
df_date['Date'] = pd.date_range('2000-01-01', '2020-03-29')
df_date['Lunar'] = df_date['Date'].apply(get_phase)
df_date.set_index('Date', inplace=True)

# %%
df_all = pd.DataFrame()
for filename in filenames:
    country_name = filename.replace('.csv', '')
    data = pd.read_csv(filename, parse_dates=['Date'])
    data = data.iloc[::-1]
    data.set_index('Date', inplace=True)
    data['Price'] = data['Price'].astype(str).str.replace(',', '').astype(float)
    data['Return'] = data['Price']/data['Price'].shift(1) - 1

    iterable = [[country_name], df_date.index]
    index = pd.MultiIndex.from_product(iterable, names=['Country', 'Date'])
    df = pd.DataFrame(columns=['Lunar'])
    df['Lunar'] = df_date['Lunar']
    df = df.join(data, how='left')
    df.drop(['Open', 'High', 'Low', 'Change %', 'Vol.'], axis=1, inplace=True)
    df.set_index(index, inplace=True)

    df_all = df_all.append(df)


print(df_all)

# %%
# Volume
df_all = pd.DataFrame()
for filename in filenames:
    country_name = filename.replace('.csv', '')
    data = pd.read_csv(filename, parse_dates=['Date'])
    data= data.iloc[::-1]
    data.set_index('Date', inplace=True)
    data = data[data['Vol.'] != '-']
    data['Vol.'] = data['Vol.'].str.replace('.', '')
    data['Vol.'] = data['Vol.'].str.replace(',', '')
    data['Vol.'] = data['Vol.'].str.replace('K', '0')
    data['Vol.'] = data['Vol.'].str.replace('M', '0000')
    data['Vol.'] = data['Vol.'].str.replace('B', '0000000')
    data['Vol.'] = data['Vol.'].astype(int)
    data['Change_Vol'] = data['Vol.']/data['Vol.'].shift(1) - 1
    
    iterable = [[country_name], df_date.index]
    index = pd.MultiIndex.from_product(iterable, names=['Country', 'Date'])
    df = pd.DataFrame(columns=['Lunar'])
    df['Lunar'] = df_date['Lunar']
    df = df.join(data, how='left')
    df.drop(['Open', 'High', 'Low', 'Change %'], axis=1, inplace=True)
    df.set_index(index, inplace=True)

    df_all = df_all.append(df)


# %%

model = PanelOLS(df_all['Return'], df_all['Lunar'], entity_effects=True)
result = model.fit()

# %%
model = PanelOLS(df_all['Change_Vol'], df_all['Lunar'], entity_effects=True)
result = model.fit()

# %%
f = open('pooled_ols_vol.tex', 'w')
f.write(result.summary.as_latex())
f.close()

# %%
