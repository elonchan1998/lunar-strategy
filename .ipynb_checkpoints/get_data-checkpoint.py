#%%
import pandas as pd
import numpy as np
import pandas_datareader.data as web
import datetime


def get_data():
    start = datetime.date(1927, 12, 1)
    end = datetime.date.today()

    df = web.DataReader("000001.SS", "yahoo", start, end)
    df.to_csv("SS.csv")
    print("Saved to current directory as: \"SS.csv\")")

if __name__ == "__main__":
    get_data()

# %%
