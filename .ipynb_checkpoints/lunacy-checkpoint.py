#%%
import pandas as pd
import numpy as np
import pylunar
import datetime
import matplotlib.pyplot as plt

def read_csv():
    df = pd.read_csv("hsi.csv", index_col=0, parse_dates=True)
    return df

def get_phase(x):
    mi.update(x)
    return mi.fractional_phase()

def sharpe(x):
    return x.mean() / x.std() * np.sqrt(252)

if __name__ == "__main__":
    df = read_csv()
# %%
    # Hong Kong ((22, 22, 62), (113, 97, 15))
    # New Yotk ((40, 73, 6), (-73, 93, 52))
    mi = pylunar.MoonInfo((40,73,6), (-73, 93, 52))
    strat = 'bfsn'  # Buy at full sell at new

    df = df.resample('1D').last()
    df.reset_index(inplace=True)
    df["phase"] = df['Date'].apply(get_phase)
    df["phase_name"] = 'Other'
    df.loc[(df['phase'] > df['phase'].shift(1)) & (df['phase'] > df['phase'].shift(-1)), 'phase_name'] = 'FULL_MOON'
    df.loc[(df['phase'] < df['phase'].shift(1)) & (df['phase'] < df['phase'].shift(-1)), 'phase_name'] = 'NEW_MOON'
    df['pos'] = np.nan
    if strat == 'bnsf':
        df.loc[df['phase_name'].shift(7) == 'NEW_MOON','pos'] = 1
        df.loc[df['phase_name'].shift(-7) == 'NEW_MOON','pos'] = 0
        #df.loc[df['phase_name'] == 'FULL_MOON', 'pos'] = 0
    elif strat == 'bfsn':
        #df.loc[df['phase_name'] == 'NEW_MOON','pos'] = 0
        df.loc[df['phase_name'].shift(7) == 'FULL_MOON', 'pos'] = 1
        df.loc[df['phase_name'].shift(-7) == 'FULL_MOON', 'pos'] = 0

    df['pos'] = df['pos'].ffill()

    df.dropna(inplace=True)
    df.reset_index(inplace=True)

    df['HSI_pnl'] = df['Close'] - df['Close'].shift(1)
    df['Strat_pnl'] = df['pos'] * df['HSI_pnl']

    df['HSI_cum_pnl'] = df['HSI_pnl'].cumsum()
    df['Strat_cum_pnl'] = df['Strat_pnl'].cumsum()

    df.plot(y=['HSI_cum_pnl', 'Strat_cum_pnl'])
    sharpe_hsi = sharpe(df['HSI_pnl'])
    sharpe_strat = sharpe(df['Strat_pnl'])

    print(sharpe_hsi, sharpe_strat)

# Paper idea
# significant but backtest not work, why?
# control weather
# HSI/China

# 4140
# horse idea
# softmax regression
# SVM
# after prediction, use kelly formula
# horse blood line, jockey, trainer, normalized finished time

# 4130
# bert + sentiment analysis



# #%%
#     position = 0
#     principal = 1000
#     bnh_principal = 1000
#     pnl = [1000]
#     bnh_pnl = [1000]
#     for i in range(1, len(signal)):
#         if signal[i] == 1 and position == 0:
#             buy_price = df.iloc[i].Close
#             position = 1
#             pnl.append(principal)
#             bnh_principal = bnh_principal*(df.iloc[i].Close/df.iloc[i-1].Close)
#             bnh_pnl.append(bnh_principal)
#         elif signal[i] == -1 and position == 1:
#             sell_price = df.iloc[i].Close
#             change = (sell_price/buy_price) - 1
#             position = 0
#             principal = principal*(1+change)
#             pnl.append(principal)
#             bnh_principal = bnh_principal*(df.iloc[i].Close/df.iloc[i-1].Close)
#             bnh_pnl.append(bnh_principal)
#         else:
#             signal[i] == 0
#             pnl.append(principal)
#             bnh_principal = bnh_principal*(df.iloc[i].Close/df.iloc[i-1].Close)
#             bnh_pnl.append(bnh_principal)
#             continue
#     df = df.assign(bnh_pnl=bnh_pnl)
#     df = df.assign(pnl=pnl)


# # %%
#     # Plot Area
#     plt.plot(np.arange(len(pnl)), pnl, 'b')
#     plt.plot(np.arange(len(bnh_pnl)), bnh_pnl, 'r')
#     plt.show()

# # %%


# %%
